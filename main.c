/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/21 11:57:36 by akarahan          #+#    #+#             */
/*   Updated: 2022/03/02 16:30:09 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	main(int ac, char *av[])
{
	char	*av_buf;
	t_arr	*arr;

	if (ac > 1)
	{
		av_buf = parser(ac, av);
		arr = (t_arr *)malloc(sizeof(t_arr));
		args_checker(av_buf, arr);
		ft_print_arr(arr->av_ints, arr->len);
		ft_qsort(arr->av_ints, 0, arr->len - 1);
		ft_print_arr(arr->av_ints, arr->len);
		ft_printf("mid = %d\n", arr->av_ints[arr->len / 2]);
		free(av_buf);
	}
	return (0);
}
