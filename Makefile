CC = gcc
CFLAGS = -Wall -Werror -Wextra -g

NAME = push_swap
SRCS = main.c \
		handle_error.c \
		parser.c \
		args_checker.c
OBJS = $(SRCS:.c=.o)
LIBFT_DIR = libft
LIBFT_NAME = libft.a
LIBS = -L$(LIBFT_DIR) -lft

all: $(LIBFT_DIR)/$(LIBFT_NAME) $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

$(LIBFT_DIR)/$(LIBFT_NAME):
	make -sC $(LIBFT_DIR)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	make -sC $(LIBFT_DIR) clean
	rm -rf $(OBJS)

fclean: clean
	make -sC $(LIBFT_DIR) fclean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
