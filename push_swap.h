/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/21 11:58:13 by akarahan          #+#    #+#             */
/*   Updated: 2022/03/02 15:18:20 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <stdio.h>
# include "./libft/includes/libft.h"

typedef struct s_node
{
	int	num;
	int	idx;
}			t_node;

typedef struct s_arr
{
	int	*av_ints;
	int	len;
}			t_arr;

typedef struct s_all
{
	t_node	*stack_a;
	t_node	*stack_b;
	int		min_a;
	int		med;
	int		len_a;
	int		len_b;
}			t_all;

/* handle_error.c */
void	handle_error(void);

/* parser.c */
char	*parser(int ac, char *av[]);

/* args_checker.c */
void	args_checker(char *av_buf, t_arr *arr);

#endif
