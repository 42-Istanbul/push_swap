#include "push_swap.h"

char	*parser(int ac, char *av[])
{
	char	*av_buf;
	char	*tmp;
	int		i;

	i = 1;
	av_buf = ft_strdup("");
	while (i < ac)
	{
		tmp = av_buf;
		av_buf = ft_strjoin(av_buf, av[i]);
		free(tmp);
		tmp = av_buf;
		av_buf = ft_strjoin(av_buf, " ");
		free(tmp);
		++i;
	}
	return (av_buf);
}
